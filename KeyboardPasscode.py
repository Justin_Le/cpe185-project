import RPi.GPIO as GPIO
import time
import subprocess

timer = time.time()
passcode = "banana" # Passcode can be changed for future purposes
attempts = 3
attemptCnt = 1

# Initializes the pins
redPin = 17
greenPin = 27

# Setup the pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(redPin, GPIO.OUT)
GPIO.setup(greenPin, GPIO.OUT)

# Initializes the pins to be set to active low
GPIO.output(redPin, GPIO.LOW)
GPIO.output(greenPin, GPIO.LOW)

# Calculates the time it taked the user to type in first input
start = time.time()
word = raw_input("Enter passcode: ")
finish = time.time()
deltaT = finish - start

# Timer begins when the first inputted word is incorrect
while (time.time() - timer < 3 + deltaT and attemptCnt < attempts and word != passcode):    # Timer can count to a smaller number to showcase timer case more quickly
    GPIO.output(redPin, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(redPin, GPIO.LOW)
    attemptCnt = attemptCnt + 1
    word = raw_input("Incorrect passcode. Please try again: ")

if (time.time() - start >= 3 + deltaT and word != passcode):
    print("Time ran out! Calling the police...")
elif (attemptCnt == attempts and word != passcode):
    print("Ran out of attempts! Calling the police...")
elif (word == passcode):
        GPIO.output(greenPin, GPIO.HIGH)
        print("Welcome home!")
        subprocess.call(["python", "ServoLock.py"])
        # Include GPIO pins here
time.sleep(3)
GPIO.output(greenPin, GPIO.LOW)