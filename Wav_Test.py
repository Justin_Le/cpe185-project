# External module imports
import RPi.GPIO as GPIO
import pygame

# Pin Definitions:
sensor1 = 11
sensor2 = 13
sensor3 = 15

# Pin Setup:
GPIO.setmode(GPIO.BOARD)
GPIO.setup(sensor1,GPIO.IN)
GPIO.setup(sensor2,GPIO.IN)
GPIO.setup(sensor3,GPIO.IN)

answer = input("What sound do you want to hear? whale, bird, or cat?")

while 1:
    if answer == "whale":
        #Output WAV1
        print("sensor 1")
        pygame.mixer.init()
        pygame.mixer.music.load("whale.wav")
        pygame.mixer.music.play()
        break
    elif answer == "bird":
        #Output WAV2
        print("sensor 2")
        pygame.mixer.init()
        pygame.mixer.music.load("bird.wav")
        pygame.mixer.music.play()
        break
    elif answer == "cat":
        #Output WAV3
        print("sensor 3")
        pygame.mixer.init()
        pygame.mixer.music.load("cat_big_x.wav")
        pygame.mixer.music.play()
        break


