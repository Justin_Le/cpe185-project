import RPi.GPIO as GPIO, time, os

DEBUG = 1
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)

def Force (RCpin):
  reading = 0
  GPIO.setup(RCpin, GPIO.OUT)
  GPIO.output(RCpin, GPIO.LOW)
  time.sleep(1)
  GPIO.setup(RCpin, GPIO.IN)

  while (GPIO.input(RCpin) ==GPIO.LOW):
    reading += 1
  return reading

while True:
  sensorvalue = Force(13)
  if sensorvalue< 100:
    print("switch on light")
    GPIO.output(11, True)
    lightstate = "ON"
    time.sleep(1)
  else:
    print("switch off light")
    GPIO.output(11, False)
    lightstate = "OFF"
    time.sleep(1)

  print(sensorvalue)
#return lightstate
