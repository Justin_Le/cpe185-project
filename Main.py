import RPi.GPIO as GPIO
import time
from picamera
from gpiozero import Button
import subprocess
from gpiozero import MotionSensor
from datetime import datetime
import os

def ForceSensor():
	# External module imports
	#import simpleaudio as sa

	# Pin Definitons:
	forcesensorPin = 17 # Broadcom pin 17 (PI pin 11)
	filename = 'firealarm.wav'
	wave_obj = sa.wave_obj.from_wave_file(firealarm.wav)


	# Pin Setup:
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(forcesensorPin, GPIO.IN)


	# Initial state for FORCE:
	GPIO.output(wave_obj, GPIO.HIGH)
	try:
	    while 1:
	        if GPIO.input(forcesensorPin):
	            play_obj = wave_obj.play()

	except KeyboardInterrupt: # If CTRL+C is pressed, exit and clean
	    GPIO.cleanup() # cleanup all GPIO

def ImageOnInput():
	button = Button(17)
	camera = PiCamera()

	camera.start_preview()
	button.wait_for_press()
	camera.capture('/home/pi/image.jpg')
	camera.stop_preview()

def KeyboardPasscode():
	timer = time.time()
	passcode = "banana" # Passcode can be changed for future purposes
	attempts = 3
	attemptCnt = 1

	# Initializes the pins
	redPin = 17
	greenPin = 27

	# Setup the pins
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(redPin, GPIO.OUT)
	GPIO.setup(greenPin, GPIO.OUT)

	# Initializes the pins to be set to active low
	GPIO.output(redPin, GPIO.LOW)
	GPIO.output(greenPin, GPIO.LOW)

	# Calculates the time it taked the user to type in first input
	start = time.time()
	word = raw_input("Enter passcode: ")
	finish = time.time()
	deltaT = finish - start

	# Timer begins when the first inputted word is incorrect
	while (time.time() - timer < 3 + deltaT and attemptCnt < attempts and word != passcode):    # Timer can count to a smaller number to showcase timer case more quickly
	    GPIO.output(redPin, GPIO.HIGH)
	    time.sleep(0.5)
	    GPIO.output(redPin, GPIO.LOW)
	    attemptCnt = attemptCnt + 1
	    word = raw_input("Incorrect passcode. Please try again: ")

	if (time.time() - start >= 3 + deltaT and word != passcode):
	    print("Time ran out! Calling the police...")
	elif (attemptCnt == attempts and word != passcode):
	    print("Ran out of attempts! Calling the police...")
	elif (word == passcode):
	        GPIO.output(greenPin, GPIO.HIGH)
	        print("Welcome home!")
	        subprocess.call(["python", "ServoLock.py"])
	        # Include GPIO pins here
	time.sleep(3)
	GPIO.output(greenPin, GPIO.LOW)

def MultipleStillImages():
	camera = PiCamera()

	camera.start_preview()
	for i in range(5):
	    sleep(5)
	    camera.capture('/home/pi/Desktop/image%s.jpg' % i)
	camera.stop_preview()

def PIRMotionDeterctor():
	camera = PiCamera()
	pir = MotionSensor(4)

	while True: #Infinite Loop
	    filename = "motion.h264" #file will be located the same place as this program
	    pir.wait_for_motion() #camera is on as long as there is motion
	    print("Motion Detected")  #print is place holder for LCD
	    camera.start_recording(filename)
	    pir.wait_for_no_motion()
	    camera.stop_recording()

def RecordVideo():
	with picamera.PiCamera() as camera:    
	    camera.start_preview()
	    camera.start_recording('/home/pi/Desktop/video.h264')
	    time.sleep(30)
	    camera.stop_recording()
	    camera.stop_preview()

def ServoLock():
	print("Ok boomer :/")

	P_SERVO = 22 # adapt to your wiring
	fPWM = 50  # Hz (not higher with software PWM)
	a = 10
	b = 2

	def setup():
	    global pwm
	    GPIO.setmode(GPIO.BOARD)
	    GPIO.setup(P_SERVO, GPIO.OUT)
	    pwm = GPIO.PWM(P_SERVO, fPWM)
	    pwm.start(0)

	def setDirection(direction):
	    duty = a / 180 * direction + b
	    pwm.ChangeDutyCycle(duty)
	    print("direction = ", direction, "-> duty = ", duty)
	    time.sleep(1) # allow to settle
	  
	print("Starting")
	setup()
	for direction in range(0, 181, 10):
	    setDirection(direction)
	direction = 0    
	setDirection(0)    
	GPIO.cleanup() 
	print("Done")

def FSR():
	DEBUG = 1
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(11, GPIO.OUT)

	def Force (RCpin):
		reading = 0
		GPIO.setup(RCpin, GPIO.OUT)
		GPIO.output(RCpin, GPIO.LOW)
		time.sleep(1)
		GPIO.setup(RCpin, GPIO.IN)

		while (GPIO.input(RCpin) ==GPIO.LOW):
	    	reading += 1
	  	return reading

		while True:
	  		sensorvalue = Force(13)
	  	if sensorvalue< 100:
		    print("switch on light")
		    GPIO.output(11, True)
		    lightstate = "ON"
		    time.sleep(1)
	  	else:
		    print("switch off light")
		    GPIO.output(11, False)
		    lightstate = "OFF"
		    time.sleep(1)
	  	print(sensorvalue)
		#return lightstate

def Photor():
	DEBUG = 1
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(11, GPIO.OUT)

	def RCtime (RCpin):
	  	reading = 0
	  	GPIO.setup(RCpin, GPIO.OUT)
	  	GPIO.output(RCpin, GPIO.LOW)
	  	time.sleep(1)
	  	GPIO.setup(RCpin,GPIO.IN)

	  	while (GPIO.input(RCpin) == GPIO.LOW):
	    	reading += 1
	  	return reading

		while True:
	  	sensorvalue = RCtime(12)

	  	if sensorvalue > 1000:
	    	print ("swtich on light")
	    	GPIO.output(11, True)
	    	lightstate ="ON"
	    	time.sleep(1)
	  	else:
	    	print ("swtich off light")
	    	GPIO.output(11, False)
	    	lightstate ="OFF"
	    	time.sleep(1)
	  	print (sensorvalue)
	  	#return lightstate

def main():
	# Programs are. to be called by their functions here

# Runs the main program
if __name__ == "__main__":
	main()
