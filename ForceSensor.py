# External module imports
import RPi.GPIO as GPIO
import time
#import simpleaudio as sa

# Pin Definitons:
forcesensorPin = 17 # Broadcom pin 17 (PI pin 11)
filename = 'firealarm.wav'
wave_obj = sa.wave_obj.from_wave_file(firealarm.wav)


# Pin Setup:
GPIO.setmode(GPIO.BCM)
GPIO.setup(forcesensorPin, GPIO.IN)


# Initial state for FORCE:
GPIO.output(wave_obj, GPIO.HIGH)
try:
    while 1:
        if GPIO.input(forcesensorPin):
            play_obj = wave_obj.play()

except KeyboardInterrupt: # If CTRL+C is pressed, exit and clean
    GPIO.cleanup() # cleanup all GPIO